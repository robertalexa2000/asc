/*
 * Tema 2 ASC
 * 2021 Spring
 */
#include <errno.h>

#include "utils.h"

/*
 * Add your unoptimized implementation here
 */

int alloc_matrices(double **AB, double **ABBt, double **AtA, double **sum,
		    int N)
{
	*AB = calloc(N * N, sizeof(double));
	if (!*AB)
		return -ENOMEM;

	*ABBt = calloc(N * N, sizeof(double));
	if (!*ABBt)
		return -ENOMEM;

	*AtA = calloc(N * N, sizeof(double));
	if (!*AtA)
		return -ENOMEM;

	*sum = calloc(N * N, sizeof(double));
	if (!*sum)
		return -ENOMEM;

	return 0;
}

double* my_solver(int N, double *A, double* B)
{
	printf("NEOPT SOLVER\n");

	int i, j, k;
	double *AB, *ABBt, *AtA, *sum;
	
	if (alloc_matrices(&AB, &ABBt, &AtA, &sum, N))
		return NULL;

	/* AB */
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			for (k = i; k < N; k++)
				*(AB + (i * N + j)) += *(A + (i * N + k)) *
						       *(B + (k * N + j));

	/* ABBt */
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			for (k = 0; k < N; k++)
				*(ABBt + (i * N + j)) += *(AB + (i * N + k)) *
							 *(B + (j * N + k));

	/* AtA */
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			for (k = 0; k <= i && k <= j; k++)
				*(AtA + (i * N + j)) += *(A + (k * N + i)) *
							*(A + (k * N + j));

	/* ABBt + AtA */
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			*(sum + (i * N + j)) = *(ABBt + (i * N + j)) +
					       *(AtA + (i * N + j));

	free(AtA);
	free(ABBt);
	free(AB);

	return sum;
}
