/*
 * Tema 2 ASC
 * 2021 Spring
 */
#include <errno.h>

#include "utils.h"

/*
 * Add your optimized implementation here
 */

int alloc_matrices(double **AB, double **ABBt, double **AtA, double **sum,
		   int N)
{
	*AB = calloc(N * N, sizeof(double));
	if (!*AB)
		return -ENOMEM;

	*ABBt = calloc(N * N, sizeof(double));
	if (!*ABBt)
		return -ENOMEM;

	*AtA = calloc(N * N, sizeof(double));
	if (!*AtA)
		return -ENOMEM;

	*sum = calloc(N * N, sizeof(double));
	if (!*sum)
		return -ENOMEM;

	return 0;
}

double* my_solver(int N, double *A, double* B)
{
	printf("OPT SOLVER\n");

	int mat_size = N * N;
	double *AB, *ABBt, *AtA, *S;	
	if (alloc_matrices(&AB, &ABBt, &AtA, &S, N))
		return NULL;

	/* AB - loop reordering && loop unrolling */
	for (register int i = 0; i < N; i++) {
		for (register int k = i; k < N; k++) {
			register double *psum, *p1, *p2;
			psum = AB + (i * N);
			p1 = A + (i * N + k);
			p2 = B + (k * N);

			for (register int j = 0; j < N; j += 4) {
				*psum += *p1 * *p2;
				psum++;
				p2++;

				*psum += *p1 * *p2;
				psum++;
				p2++;

				*psum += *p1 * *p2;
				psum++;
				p2++;

				*psum += *p1 * *p2;
				psum++;
				p2++;
			}
		}
	}

	/* ABBt - loop unrolling */
	for (register int i = 0; i < N; i++) {
		for (register int j = 0; j < N; j++) {
			register double sum = 0;
			register double *psum, *p1, *p2;
			psum =  ABBt + (i * N + j);
			p1 = AB + (i * N);
			p2 = B + (j * N);

			for (register int k = 0; k < N; k += 4) {
				sum += *p1 * *p2;
				p1++;
				p2++;

				sum += *p1 * *p2;
				p1++;
				p2++;

				sum += *p1 * *p2;
				p1++;
				p2++;

				sum += *p1 * *p2;
				p1++;
				p2++;
			}

			*psum = sum;
		}
	}

	/* AtA - loop reordering */
	for (register int i = 0; i < N; i++) {
		for (register int k = 0; k <= i; k++) {
			double *psum, *p1, *p2;
			psum = AtA + (i * N + k);
			p1 = A + (k * N + i);
			p2 = A + (k * N + k);

			for (register int j = k; j < N; j++) {
				*psum += *p1 * *p2;
				psum++;
				p2++;
			}
		}
	}

	/* ABBt + AtA - loop unrolling */
	register double *psum, *p1, *p2;
	psum = S;
	p1 = ABBt;
	p2 = AtA;
	for (register int i = 0; i < mat_size; i += 4) {
		*psum = *p1 + *p2;
		psum++;
		p1++;
		p2++;

		*psum = *p1 + *p2;
		psum++;
		p1++;
		p2++;

		*psum = *p1 + *p2;
		psum++;
		p1++;
		p2++;

		*psum = *p1 + *p2;
		psum++;
		p1++;
		p2++;
	}
		
	free(AtA);
	free(ABBt);
	free(AB);

	return S;
}
