/*
 * Tema 2 ASC
 * 2021 Spring
 */
#include <errno.h>
#include <cblas.h>

#include "utils.h"

/*
 * Add your BLAS implementation here
 */

int alloc_matrices(double **AB, double **ABBt, double **AtA, double **sum,
		   int N)
{
	*AB = calloc(N * N, sizeof(double));
	if (!*AB)
		return -ENOMEM;

	*ABBt = calloc(N * N, sizeof(double));
	if (!*ABBt)
		return -ENOMEM;

	*AtA = calloc(N * N, sizeof(double));
	if (!*AtA)
		return -ENOMEM;

	*sum = calloc(N * N, sizeof(double));
	if (!*sum)
		return -ENOMEM;

	return 0;
}

double* my_solver(int N, double *A, double* B)
{
	printf("BLAS SOLVER\n");

	double *AB, *ABBt, *AtA, *sum;	
	if (alloc_matrices(&AB, &ABBt, &AtA, &sum, N))
		return NULL;

	/* AB */
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1, A, N, B, N, 0, AB, N);

	/* ABBt */
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, N, N, N, 1, AB, N, B, N, 0, ABBt, N);

	/* AtA */
	cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, N, N, N, 1, A, N, A, N, 0, AtA, N);

	/* ABBt + AtA */
	cblas_dcopy(N * N, (const double *) ABBt, 1, sum, 1);
	cblas_daxpy(N * N, 1, (const double *) AtA, 1, sum, 1);

	free(AtA);
	free(ABBt);
	free(AB);

	return sum;
}
