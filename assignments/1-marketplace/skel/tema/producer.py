"""
This module represents the Producer.

Computer Systems Architecture Course
Assignment 1
March 2021
"""

from threading import Thread
import time


class Producer(Thread):
    """
    Class that represents a producer.
    """

    def __init__(self, products, marketplace, republish_wait_time, **kwargs):
        """
        Constructor.

        @type products: List()
        @param products: a list of products that the producer will produce

        @type marketplace: Marketplace
        @param marketplace: a reference to the marketplace

        @type republish_wait_time: Time
        @param republish_wait_time: the number of seconds that a producer must
        wait until the marketplace becomes available

        @type kwargs:
        @param kwargs: other arguments that are passed to the Thread's __init__()
        """
        Thread.__init__(self, **kwargs)

        self.products = products
        self.marketplace = marketplace
        self.republish_wait_time = republish_wait_time

    def run(self):
        producer_id = self.marketplace.register_producer()
        producer_no = int(producer_id)

        while True:
            for product in self.products:
                for _ in range(product[1]):
                    # acquire lock
                    self.marketplace.locks[producer_no].acquire()

                    if self.marketplace.publish(producer_id, product[0]) is False:
                        self.marketplace.locks[producer_no].release()
                        time.sleep(self.republish_wait_time)
                        self.marketplace.locks[producer_no].acquire()

                    # release lock
                    self.marketplace.locks[producer_no].release()

                    # wait
                    time.sleep(int(product[2]))
