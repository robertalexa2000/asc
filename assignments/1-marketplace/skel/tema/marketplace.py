"""
This module represents the Marketplace.

Computer Systems Architecture Course
Assignment 1
March 2021
"""

from threading import Lock


class Marketplace:
    """
    Class that represents the Marketplace. It's the central part of the implementation.
    The producers and consumers use its methods concurrently.
    """
    def __init__(self, queue_size_per_producer):
        """
        Constructor

        :type queue_size_per_producer: Int
        :param queue_size_per_producer: the maximum size of a queue associated with each producer
        """
        self.queue_size_per_producer = queue_size_per_producer
        self.locks = []
        self.producers_queues = {}
        self.consumers_carts = {}
        self.producers_lock = Lock()
        self.consumers_lock = Lock()

    def register_producer(self):
        """
        Returns an id for the producer that calls this.
        """
        with self.producers_lock:
            registration_string = str(len(self.producers_queues.keys()))
            self.producers_queues[registration_string] = []
            self.locks.append(Lock())

        return registration_string

    def publish(self, producer_id, product):
        """
        Adds the product provided by the producer to the marketplace

        :type producer_id: String
        :param producer_id: producer id

        :type product: Product
        :param product: the Product that will be published in the Marketplace

        :returns True or False. If the caller receives False, it should wait and then try again.
        """
        if len(self.producers_queues[producer_id]) < self.queue_size_per_producer:
            self.producers_queues[producer_id].append(product)
            return True

        return False

    def new_cart(self):
        """
        Creates a new cart for the consumer

        :returns an int representing the cart_id
        """
        with self.consumers_lock:
            new_cart_id = len(self.consumers_carts.keys())

        self.consumers_carts[new_cart_id] = []
        return new_cart_id

    def add_to_cart(self, cart_id, product):
        """
        Adds a product to the given cart. The method returns

        :type cart_id: Int
        :param cart_id: id cart

        :type product: Product
        :param product: the product to add to cart

        :returns True or False. If the caller receives False, it should wait and then try again
        """
        with self.consumers_lock:
            no_keys = len(self.producers_queues.keys())

        for producer_id in range(no_keys):
            # get the lock
            self.locks[producer_id].acquire()

            # search in each producer's queue
            for produced_product in self.producers_queues[str(producer_id)]:
                if product == produced_product:
                    self.consumers_carts[cart_id].append(product)
                    self.producers_queues[str(producer_id)].remove(
                        produced_product)
                    self.locks[producer_id].release()
                    return True

            # release the lock
            self.locks[producer_id].release()

        return False

    def remove_from_cart(self, cart_id, product):
        """
        Removes a product from cart.

        :type cart_id: Int
        :param cart_id: id cart

        :type product: Product
        :param product: the product to remove from cart
        """
        # remove the product
        self.consumers_carts[cart_id].remove(product)

        # for convenience, add it to producer's 0 queue
        with self.locks[0]:
            self.producers_queues[str(0)].append(product)

    def place_order(self, cart_id):
        """
        Return a list with all the products in the cart.

        :type cart_id: Int
        :param cart_id: id cart
        """
        for product in self.consumers_carts[cart_id]:
            print ("cons%d bought %s" % (cart_id + 1, product))
        self.consumers_carts[cart_id] = []
