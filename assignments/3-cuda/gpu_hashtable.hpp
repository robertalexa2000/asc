#ifndef _HASHCPU_
#define _HASHCPU_

#include <vector>

using namespace std;

#define cudaCheckError() { \
	cudaError_t e=cudaGetLastError(); \
	if(e!=cudaSuccess) { \
		cout << "Cuda failure " << __FILE__ << ", " << __LINE__ << ", " << cudaGetErrorString(e); \
		exit(0); \
	 }\
}

#define MIN_LOAD_FACTOR 0.8
#define MAX_LOAD_FACTOR 0.9

struct Pair {
	int key;
	int value;
};

class GpuHashTable
{
	public:
		struct Pair *hashTable;
		int capacity;
		int *noElems;

		GpuHashTable(int size);
		void reshape(int sizeReshape);
		
		bool insertBatch(int *keys, int* values, int numKeys);
		int* getBatch(int* key, int numItems);
	
		~GpuHashTable();
};

#endif

