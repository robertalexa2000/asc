#include <iostream>
#include <limits.h>
#include <stdlib.h>
#include <ctime>
#include <sstream>
#include <string>
#include <stdio.h>

#include "test_map.hpp"
#include "gpu_hashtable.hpp"

using namespace std;

/* Init function */
GpuHashTable::GpuHashTable(int size)
{
	glbGpuAllocator->_cudaMallocManaged((void **) &hashTable,
										size * sizeof(struct Pair));
    cudaCheckError();
    cudaMemset(hashTable, 0, size * sizeof(struct Pair));
    cudaCheckError();

	capacity = size;

    glbGpuAllocator->_cudaMallocManaged((void **) &noElems, sizeof(int));
    cudaCheckError();
	*noElems = 0;
}

/* Destructor */
GpuHashTable::~GpuHashTable()
{
    glbGpuAllocator->_cudaFree(noElems);
    cudaCheckError();
    glbGpuAllocator->_cudaFree(hashTable);
    cudaCheckError();
}

__device__ unsigned int hashFunction(unsigned int key, int size) {
    key = ((key >> 16) ^ key) * 0x45d9f3b;
    key = ((key >> 16) ^ key) * 0x45d9f3b;
    key = (key >> 16) ^ key;
	key %= size;

    return key;
}

__global__ void kernel_reshape(union Entry *newHashTable, int newCapacity, union Entry* oldHashTable, int oldCapacity)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    struct Pair oldPair, newPair;
    int pos;

    /* started more threads than necessary */
    if (idx >= oldCapacity)
        return;

    /* assign the element to insert */
    newEntry.raw = oldHashTable[idx].raw;
    if (newEntry.raw == 0)
        return;

    /* find the new pos */
    pos = hashFunction(newEntry.pair.key, newCapacity);

    /* insert using atomic operations */
    oldEntry.raw = atomicExch(&newHashTable[pos].raw, newEntry.raw);
    while (oldEntry.raw) {
        atomicExch(&newHashTable[pos].raw, oldEntry.raw);
        pos = (pos + 1) % newCapacity;
        oldEntry.raw = atomicExch(&newHashTable[pos].raw, newEntry.raw);
    }
}

/* Performs resize of the hashtable based on load factor */
void GpuHashTable::reshape(int numBucketsReshape) {
    union Entry *hashTableReshape;
    int num_threads, num_blocks;

    /* alloc a new hashTable */
    glbGpuAllocator->_cudaMallocManaged((void **) &hashTableReshape, numBucketsReshape * sizeof(union Entry));
    cudaCheckError();
    cudaMemset(hashTableReshape, 0, numBucketsReshape * sizeof(union Entry));
    cudaCheckError();

    /* count the number of blocks */
    num_threads = 256;
    num_blocks = capacity / num_threads;
    if (capacity % num_threads != 0)
        num_blocks++;

    /* start the kernel routine */
    kernel_reshape<<<num_blocks, num_threads>>>(hashTableReshape, numBucketsReshape, hashTable, capacity);
    cudaDeviceSynchronize();
    cudaCheckError();
    
    /* reassign pointers and free memory */
    glbGpuAllocator->_cudaFree(hashTable);
    cudaCheckError();
    hashTable = hashTableReshape;
    capacity = numBucketsReshape;
}

/* kernel insert function */
__global__ void kernel_insert(union Entry *hashTable, int *keys, int *values, int numKeys, int *noElems, int capacity)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int pos;
	union Entry newEntry, oldEntry;

	/* check if preconditions are met */
	if (idx >= numKeys)
		return;
	if (keys[idx] <= 0 || values [idx] <= 0)
		return;

	/* save the key and value to insert */
	newEntry.pair.key = keys[idx];
	newEntry.pair.value = values[idx];

	/* calculate the position */
	pos = hashFunction(keys[idx], capacity);

    /* insert using atomic operations */
    while ((oldEntry.raw = atomicExch(&hashTable[pos].raw, newEntry.raw))) {
        if (oldEntry.pair.key == newEntry.pair.key)
            break;

	    atomicExch(&hashTable[pos].raw, oldEntry.raw);
	    pos = (pos + 1) % capacity;
    }

    if (oldEntry.pair.key != newEntry.pair.key)
        atomicAdd(noElems, 1);

    // oldEntry.raw = atomicExch(&hashTable[pos].raw, newEntry.raw);
    // while (oldEntry.raw) {
    //     /* duplicate keys, just update the value */
    //     if (newEntry.pair.key == oldEntry.pair.key)
    //         break;

    //     atomicExch(&hashTable[pos].raw, oldEntry.raw);
    //     pos = (pos + 1) % capacity;
    //     oldEntry.raw = atomicExch(&hashTable[pos].raw, newEntry.raw);
    // }

    // /* update the size of the hashTable only if the key is not a duplicate */
    // if (newEntry.pair.key != oldEntry.pair.key)
    //     atomicAdd(noElems, 1);
}

/* Inserts a batch of key:value, using GPU and wrapper allocators */
bool GpuHashTable::insertBatch(int *keys, int* values, int numKeys)
{
	int num_threads, num_blocks;
    int *device_keys, *device_values;

	/* check if reshape is needed */
	if ( 1.0 * (*noElems + numKeys) / capacity >= MAX_LOAD_FACTOR)
        reshape((*noElems + numKeys) / MIN_LOAD_FACTOR);

	num_threads = 256;
	num_blocks = numKeys / 256;
	if (numKeys % 256 != 0)
		num_blocks++;

	/* init device keys */
	glbGpuAllocator->_cudaMalloc((void **) &device_keys, numKeys * sizeof(int));
    cudaCheckError();
    cudaMemcpy(device_keys, keys, numKeys * sizeof(int),
               cudaMemcpyHostToDevice);
    cudaCheckError();

	/* init device values */
	glbGpuAllocator->_cudaMalloc((void **) &device_values, numKeys * sizeof(int));
    cudaCheckError();
	cudaMemcpy(device_values, values, numKeys * sizeof(int),
               cudaMemcpyHostToDevice);
	cudaCheckError();

	kernel_insert<<<num_blocks, num_threads>>>(hashTable, device_keys, device_values, numKeys, noElems, capacity);
    cudaDeviceSynchronize();
    cudaCheckError();
    
    glbGpuAllocator->_cudaFree(device_keys);
    cudaCheckError();
    glbGpuAllocator->_cudaFree(device_values);
    cudaCheckError();

	return true;
}

__global__ void kernel_get(union Entry *hashTable, int *keys, int *values, int numKeys, int capacity)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    /* check if preconditions are met */
    if (idx >= numKeys)
        return;
    if (keys[idx] <= 0)
        return;

    /* find the reference position */
    int pos = hashFunction(keys[idx], capacity);

    /* loop until you find the key */
    while (hashTable[pos].pair.key != keys[idx])
        pos = (pos + 1) % capacity;
    
    /* store the value */
    values[idx] = hashTable[pos].pair.value;
}

/* Gets a batch of key:value, using GPU */
int* GpuHashTable::getBatch(int* keys, int numKeys) {
    int num_threads, num_blocks;
    int *device_keys, *device_values;
    int *values;

    glbGpuAllocator->_cudaMalloc((void **) &device_keys, numKeys * sizeof(int));
    cudaCheckError();
    cudaMemcpy(device_keys, keys, numKeys * sizeof(int), cudaMemcpyHostToDevice);
    cudaCheckError();
    
    glbGpuAllocator->_cudaMalloc((void **) &device_values, numKeys * sizeof(int));
    cudaCheckError();
    
    /* start kernel */
    // num_threads = 256;
    // num_blocks = numKeys / num_threads;
    // if (numKeys % num_threads != 0)
    //     num_blocks++;
    // kernel_get<<<num_blocks, num_threads>>>(hashTable, device_keys, device_values, numKeys, capacity);
    // cudaDeviceSynchronize();
    // cudaCheckError();

    values = (int *) malloc(numKeys * sizeof(int));
    DIE(values == NULL, "malloc");
    cudaMemcpy(values, device_values, numKeys * sizeof(int), cudaMemcpyDeviceToHost);
    cudaCheckError();
    
    glbGpuAllocator->_cudaFree(device_keys);
    cudaCheckError();
    glbGpuAllocator->_cudaFree(device_values);
    cudaCheckError();

    return values;
}